function ValidationError(status, code, message) {
  this.status = status || 500;
  this.code = code || -1;
  this.name = 'ValidationError';
  this.message = message || 'No message provided';
  this.stack = (new Error()).stack;
}
ValidationError.prototype = Object.create(Error.prototype);
ValidationError.prototype.constructor = ValidationError;

module.exports = ValidationError;
