FROM node:5.7.1

MAINTAINER Mathias Sandberg

RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app

# Install node dependencies
COPY package.json /usr/src/app
RUN npm install

# Copy project
COPY . /usr/src/app

EXPOSE 3000

CMD ["npm", "run", "start"]
