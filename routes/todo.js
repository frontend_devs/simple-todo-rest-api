var router = require('express').Router(),
  _ = require('underscore'),
  TodoModel = require('../models/TodoModel'),
  ValidationError = require('../util/ValidationError');


var todoDatabase = [
  new TodoModel('Ringa skatteupplysningen'),
  new TodoModel('Delta på frontend-workshop', true, new Date(2016, 3, 13, 13)),
  new TodoModel('Skriva detta API', true, new Date(2016, 3, 4)),
];

router.route('/')
/*
 * Hämtar samtliga Todo:s
 * möjlighet att filtrera
 **/
.get(function(req, res, next) {

  var filteredArray = todoDatabase;

  if(typeof req.query.complete !== 'undefined') {
    filteredArray = filteredArray.filter(function(todo) {
      return todo.complete === (req.query.complete === 'true');
    });
  }

  if (typeof req.query.title !== 'undefined') {
    var queryTitle = req.query.title.toLowerCase();

    filteredArray = filteredArray.filter(function(todo) {
      var todoTitle = todo.title.toLowerCase();
      return todoTitle.indexOf(queryTitle) >= 0;
    });
  }

    res.status(200).json(filteredArray);
})
/*
 * Skapar en ny Todo
 **/
.post(validateTodo, function(req, res, next) {
  todoDatabase.push(req.todo);
  res.status(201).json(req.todo);
})
/*
 * Uppdaterar en Todo
 **/
.put(function(req, res, next) {
  req.isUpdate = true;
  next();
},
validateTodo,
function(req, res, next) {
  var todoData = _.pick(req.body, 'title', 'complete', 'completeDate'), key;
  for(key in todoData) {
    if(!isUndefined(todoData[key])) {
      req.todo[key] = todoData[key];
    }
  }
  if(!req.todo.complete) req.todo.completeDate = null;
  res.status(200).json(req.todo);
});

router.route('/:id')
/*
 * Hämtar en Todo med givet ID
 **/
.get(function(req, res, next) {
  var todo = getTodoById(parseInt(req.params.id));
  if(!todo) return next(new ValidationError(404, 1, 'Todo:n kunde inte hittas'));
  res.status(200).json(todo);
})
/*
 * Tar bort en todo med givet ID
 **/
.delete(function(req, res, next) {
  var todo = getTodoById(parseInt(req.params.id)), index;
  if(!todo) return next(new ValidationError(404, 1, 'Todo:n kunde inte hittas'));
  index = todoDatabase.indexOf(todo);
  todoDatabase.splice(index, 1);
  res.status(200).json(todo);
});




function validateTodo(req, res, next) {
  var todoData = _.pick(req.body, 'id', 'title', 'complete', 'completeDate'),
    todo,
    err;

  // Städa bort completeDate om det felaktigt sats av klienten
  // då complete är undefined eller false.
  // Notera att complete och completeDate endast samexisterar då
  // complete === true
  if(!todoData.complete && !isUndefined(todoData.completeDate)) {
    delete todoData.completeDate;
    delete todoData.complete;
  }

  if(req.isUpdate) {
    // Validera att det finns ett id
    err = validateId(todoData.id);
    if(err) return next(err);
    // Validera att id:t existerar
    todo = getTodoById(todoData.id);
    if(!todo) {
      return next(new ValidationError(404, 1, 'Todo:n kunde inte hittas'));
    }
  }

  // Validera title
  err = validateTitle(todoData.title, req.isUpdate);
  if(err) return next(err);
  // Validera complete
  err = validateComplete(todoData.complete);
  if(err) return next(err);
  // Validera completeDate
  err = validateCompleteDate(todoData.completeDate);
  if(err) return next(err);
  // Validera att det finns ett datum om complete är true
  err = validateCompleteAndCompleteDate(todoData.complete, todoData.completeDate);
  if(err) return next(err);

  req.todo = todo || new TodoModel(todoData.title, todoData.complete, todoData.completeDate);

  next();
}

function validateId(id) {
  if(typeof id !== 'number') {
    return new ValidationError(400, 1, 'Felaktig datatyp: id, eller ej angivet');
  }
  return null;
}

function validateTitle(title, isUpdate) {
  if(isUndefined(title) && isUpdate) return null;
  if(typeof title !== 'string') {
    return new ValidationError(400, 1, 'Felaktig datatyp: title');
  }
  if(title.length < 3) {
    return new ValidationError(400, 1, 'Längden för en title måste minst vara 3 tecken');
  }

  return null;
}

function validateComplete(complete) {
  if(!isUndefined(complete) && typeof complete !== 'boolean') {
    return new ValidationError(400, 1, 'Felaktig datatyp: complete');
  }
  return null;
}

function validateCompleteDate(completeDate) {
  if(!isUndefined(completeDate) && (typeof completeDate !== 'string' || !Date.parse(completeDate))) {
    return new ValidationError(400, 1, 'Felaktigt datumformat: completeDate (Ex. på korrekt format: YYYY-MM-DDTHH:MM:SS.XXXZ)');
  }
  return null;
}

function validateCompleteAndCompleteDate(complete, completeDate) {
  if((isUndefined(complete) && !isUndefined(completeDate)) ||
  (complete && isUndefined(completeDate))) {
    return new ValidationError(400, 1, 'En Todo som ska markeras klar måste både ha datum och markeras som klar');
  }
  return null;
}

function isUndefined(value) {
  return typeof value === 'undefined';
}

function getTodoById(id) {
  var len = todoDatabase.length,
    i = 0,
    todo;
  for(; i < len; i++) {
    if(todoDatabase[i].id === id) return todoDatabase[i];
  }
  return null;
}

module.exports = router;
