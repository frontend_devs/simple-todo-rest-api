var express = require('express');
var router = express.Router();
var now = new Date(),
  startDateTime = now.toISOString().replace('T', ' ').slice(0, 19);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Todo API', startDateTime: startDateTime });
});

module.exports = router;
