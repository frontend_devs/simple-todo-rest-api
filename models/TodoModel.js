var nextId = 1;

function parseDate(date) {
  var pDate = null;

  if(date) {
    pDate = new Date(date);
  }

  return pDate;
}

function TodoModel(title, complete, completeDate) {
  this.id = nextId++;
  this.title = title;
  this.complete = !!complete;
  this.completeDate = parseDate(completeDate);
}

module.exports = TodoModel;
